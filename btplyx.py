#!/usr/bin/env python
import freenect
import cv2
import frame_convert2
import numpy as np
import os
import time

window_depth_name = 'Depth'
PEROLD=0
perimeter=0
ACT=0;
SG=0;
X=0
Y=0
XO=X
YO=Y
cir=0.
cnvx=0.

threshold = 95
current_depth = 267
current_area=2500
g1=36
g2=36
pix=150
m=0
m2=0
ACV=0.0
ACIR=0.0
AVAR=0.0
AAR=0.0
AR=0.0
AROLD=0.0


def change_g2(value):
    global g2
    g2 = value

def change_g1(value):
    global g1
    g1 = value

def change_threshold(value):
    global threshold
    threshold = value

def show_depth2():
    global threshold;#,w,p;
    global current_depth,FIM,quad,ACT,AR,SG;
    global X,Y,XO,YO;
    global current_area,AROLD,g1,g2,perimeter,cvnx
    g1x=g1/100
    g2x=g2/100
    n=0;
    global m,m2;
    th=threshold/100;
    s=''
    kernelSize = (7, 7)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernelSize)
    #####################################
    #Read depth map, mirror it and remove the background.
    depthx, timestamp = freenect.sync_get_depth()
    depthx = np.reciprocal(1.0*depthx)
    mx=max(np.ravel(depthx))
    depthx=(depthx>(th*mx)).astype(np.uint8)
    depthx = np.fliplr(depthx)
    depthx = depthx.astype(np.uint8)
    ####################################
    W=depthx.shape[1];
    H=depthx.shape[0];
    # convert the grayscale image to binary image
    ret,thresh = cv2.threshold(depthx,127,255,0)
    connectivity = 8
    output = cv2.connectedComponentsWithStats(depthx, connectivity, cv2.CV_32S)
    (numLabels, labels, stats, centroids) = output
    areas=stats[:,4]
    nr=np.arange(numLabels)
    ranked=sorted(zip(areas,nr))
    z1=ranked[:-1]
    N1=len(z1)
    depthx= np.zeros((labels.shape))
    drawing = np.zeros((depthx.shape[0], depthx.shape[1], 3), dtype=np.uint8)
    Amax=max(stats[1:,4])
    for j in range(1,numLabels):
        if stats[j][4]==Amax:
            #print(j,stats[j][4])
            depthx[labels==j]=255
            drawing[labels==j]=255
            closing = cv2.morphologyEx(depthx, cv2.MORPH_OPEN, kernel)
            retx,threshx = cv2.threshold(closing,127,255,0)
            drawing = np.zeros((depthx.shape[0], depthx.shape[1], 3), dtype=np.uint8)
            drawing[threshx==255]=255
            drawing = cv2.cvtColor(drawing, cv2.COLOR_BGR2GRAY)
            output2 = cv2.connectedComponentsWithStats(drawing.astype(np.uint8), connectivity, cv2.CV_32S)
            (numLabelsx, labelsx, statsx, centroidsx) = output2
            if numLabelsx>1:
                Amaxx=max(statsx[1:,4])
                for k in range(1,numLabelsx):
                    if statsx[k][4]==Amaxx:
                        drawing = np.zeros((depthx.shape[0], depthx.shape[1], 3), dtype=np.uint8)
                        drawingx = np.zeros((depthx.shape[0], depthx.shape[1], 3), dtype=np.uint8)
                        drawing[closing==255]=255
                        drawingx[closing==255]=255
                        cv2.circle(drawing, (int(centroidsx[k][0]),int(centroidsx[k][1])), 50, (0, 255, 0),1,8,0)
                        p1x=statsx[k][0];p1y=statsx[k][1];
                        p2x=p1x+statsx[k][2];p2y=p1y+statsx[k][3];
                        cv2.rectangle(drawing,(int(p1x),int(p1y)),(int(p2x),int(p2y)),(255,255,0),3)
                        img_gray = cv2.cvtColor(drawingx,cv2.COLOR_BGR2GRAY)
                        ret, thresh = cv2.threshold(img_gray, 127, 255,0)
                        contours, hierarchy = cv2.findContours(thresh,2,1)
                        cnt = contours[0]
                        hull=cv2.convexHull(cnt)
                        per=cv2.arcLength(hull,True)
                        ar=cv2.contourArea(hull,True)
                        cir=(4*np.pi*Amaxx)/(per**2)
                        cvnx=Amaxx/ar
                        wx=" Wbox:"+str("{:.2f}".format(statsx[k][2]))+" "
                        hx=wx+"Hbox:"+str("{:.2f}".format(statsx[k][3]))+" "
                        AROLD=AR
                        AR=float(statsx[k][2]) / float(statsx[k][3])
                        R=hx+" R:"+str("{:.2f}".format(AR))
                        print(per,ar,cir,cvnx)
                        cv2.drawContours(drawing,cnt,-1, (255,0,0),3)
                        cv2.drawContours(drawing,[hull],-1, (255,255,255),2)
                        st='CIR:'+str("{:.2f}".format(cir))+"-"+'CONV:'+str("{:.2f}".format(cvnx))
                        cv2.putText(drawing,str(st)+R,(10,H-50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))
                        cv2.putText(drawing,'ACTION:'+str(ACT),(10,50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))
                        print(len(statsx[k]))
                        break
            break
    print('*********************')

    X=(1/(1-2*g1x))*float(centroids[1][0]/W)-(g1x/(1-2*g1x));
    Y=(1/(1-2*g2x))*float(centroids[1][1]/H)-(g2x/(1-2*g2x));

    #if np.abs(X-XO)>5 or np.abs(Y-YO)>5 and X>0 and Y>0 and Y<1 and X<1:
    #    X=XO
    #    Y=Y0

    #        #print(X,Y,g1x,g2x)
    if X>=1.0:
        X=1.1
    if Y>=1.0:
        Y=1.1
    if X<=0.0:
        X=-0.1
    if Y<=0.0:
        Y=-0.1

    ACT=0

    if AR<=1.0:
        m+=1

    if AROLD<=1 and AR>1:
        if m<20:
            ACT=1
        if m>=20:
            ACT=2
        m=0

    if cvnx<0.75:
        ACT=7

    if X>0 and Y>0 and X<1 and Y<1:
        #######

        if X<=0.33:
            X=0.16
        if X>0.33 and X<=0.66:
            X=0.5
        if X>0.66:
            X=0.83

        if Y<=0.33:
            Y=0.16
        if Y>0.33 and Y<=0.66:
            Y=0.5
        if Y>0.66:
            Y=0.83
        #####################
        s=str("{:.2f}".format(X))+" "+str("{:.2f}".format(1-Y))+" "+str(ACT);
        #XO=X
        #YO=Y
    cv2.rectangle(drawing,(int(g1x*W),int(g2x*H)),(int((1-g1x)*W),int((1-g2x)*H)),(0,255,0),3)
    cv2.imshow('Depth', drawing)
    print(s)
    return s


cv2.namedWindow('Depth')
cv2.createTrackbar('threshold', 'Depth', threshold,     100,  change_threshold)
cv2.createTrackbar('g1', 'Depth', g1, 40, change_g1)
cv2.createTrackbar('g2', 'Depth', g2, 40, change_g2)

print('Press ESC in window to stop')

FIM=0;
p='fifo'
cal=0
j=0

ACVL=[]
ACRL=[]
AVARL=[]

while 1:
        a=show_depth2()
        if len(a)!=0:
            print(a)
            if j<100:
                w=open(p, 'w')
                w.write(''+'\n')
                w.flush()
                w.close()
            else:
                w=open(p, 'w')
                w.write(a+'\n')
                w.flush()
                w.close()

        j+=1
        print(j)

        if cv2.waitKey(10) == 27:
            freenect.sync_stop()
            cv2.destroyAllWindows()
            break
