import pygame
import os
import time


# This is a simple class that will help us print to the screen.
# It has nothing to do with the joysticks, just outputting the
# information. NOT IN USE CURRENTLY
class TextPrint(object):
    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def tprint(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, (self.x, self.y))
        self.y += self.line_height

    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15

    def indent(self):
        self.x += 10

    def unindent(self):
        self.x -= 10


pygame.init()

# Set the width and height of the screen (width, height).
#screen = pygame.display.set_mode((500, 500))

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates.
clock = pygame.time.Clock()

# Initialize the joysticks.
pygame.joystick.init()

# Get ready to print.
textPrint = TextPrint()

p='fifo'
DX=0.33
DY=0.33
X=0.5
Y=0.5


# -------- Main Program Loop -----------
while not done:
    #
    # EVENT PROCESSING STEP
    #
    # Possible joystick actions: JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONDOWN,
    # JOYBUTTONUP, JOYHATMOTION
    for event in pygame.event.get(): # User did something.
        if event.type == pygame.QUIT: # If user clicked close.
            done = True # Flag that we are done so we exit this loop.
        elif event.type == pygame.JOYBUTTONDOWN:
            #print("Joystick button pressed.")
            pass
        elif event.type == pygame.JOYBUTTONUP:
            #print("Joystick button released.")
            pass
    # Get count of joysticks.
    joystick_count = pygame.joystick.get_count()

    # For each joystick:
    for i in range(joystick_count):
        joystick = pygame.joystick.Joystick(i)
        joystick.init()

        try:
            jid = joystick.get_instance_id()
        except AttributeError:
            # get_instance_id() is an SDL2 method
            jid = joystick.get_id()
        # Get the name from the OS for the controller/joystick.
        name = joystick.get_name()
        # Usually axis run in pairs, up/down for one, and left/right for
        # the other.
        ACT=0
        axes = joystick.get_numaxes()
        for i in range(axes):
            axis = joystick.get_axis(i)

        buttons = joystick.get_numbuttons()

        for i in range(0,3):
            button = joystick.get_button(i)
            if button==1:
                ACT=(i+1)

        hats = joystick.get_numhats()
        # Hat position. All or nothing for direction, not a float like
        # get_axis(). Position is a tuple of int values (x, y).
        #for i in range(hats):
        hat = joystick.get_hat(0)
        #print(i,hat[0],hat[1])
        if X+DX*hat[0] > 0 and X+DX*hat[0] <1:
            X+=DX*hat[0]
        if Y+DY*hat[1] > 0 and Y+DY*hat[1] <1:
            Y+=DY*hat[1]
        print(X,Y,ACT)
        s=str("{:.2f}".format(X))+" "+str("{:.2f}".format(Y))+" "+str(ACT);
        print(s)
        w=open(p, 'w')
        w.write(s+'\n')
        w.flush()
        w.close()


    # Limit to 20 frames per second.
    clock.tick(5)

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()
