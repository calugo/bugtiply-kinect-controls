import numpy as np
import cv2
import sys
from pylibfreenect2 import Freenect2, SyncMultiFrameListener
from pylibfreenect2 import FrameType, Registration, Frame
from pylibfreenect2 import createConsoleLogger, setGlobalLogger
from pylibfreenect2 import LoggerLevel

window_depth_name = 'Depth'
NEV=1;
ACT=0;
X=0
Y=0
threshold = 3
g1=31
g2=36
m=0
m2=0
j=0
cir=0.
cvnx=0.
AR=0.

kernelSize = (7, 7)
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernelSize)

def change_g2(value):
    global g2
    g2 = value

def change_g1(value):
    global g1
    g1 = value

def change_threshold(value):
    global threshold
    threshold = value

def show_depth2():
    global kernelSize,kernel
    global threshold;#,w,p;
    global current_depth,FIM,quad,ACT,j;
    global X,Y,XO,YO;
    global current_area,PEROLD,g1,g2,pix,perimeter
    global AR,cvnx,circ
    g1x=g1/100
    g2x=g2/100
    dtip=10
    global m,m2;
    th=threshold/100;
    s=''
    frames = listener.waitForNewFrame()
    depth = frames["depth"]
    depth=depth.asarray()
    mx=max(np.ravel(depth))
    depth[depth==0]=mx
    depth=np.sqrt(depth)
    depth=depth/max(np.ravel(depth))
    minx=min(np.ravel(depth))
    depth[depth>minx+th]=0.0
    depth[depth>0]=1.0
    #depth = 255-255*depth.astype(np.uint8)
    depth = 255*depth.astype(np.uint8)
    W=depth.shape[1];
    H=depth.shape[0];
    #depth = cv2.cvtColor(depth, cv2.COLOR_BGR2GRAY)
    #ret,thresh = cv2.threshold(depth,127,255,0)
    ret,thresh=cv2.threshold(depth, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    connectivity = 8
    output = cv2.connectedComponentsWithStats(depth, connectivity, cv2.CV_32S)
    (numLabels, labels, stats, centroids) = output
    ################
    areas=stats[:,4]
    nr=np.arange(numLabels)
    ranked=sorted(zip(areas,nr))
    z1=ranked[:-1]
    N1=len(z1)
    depthx= np.zeros((labels.shape))
    drawing = np.zeros((depth.shape[0], depth.shape[1], 3), dtype=np.uint8)
    print('*********************')
    Amax=max(stats[1:,4])
    for j in range(1,numLabels):
        if stats[j][4]==Amax:
    #        #print(j,stats[j][4])
            depthx[labels==j]=255
            drawing[labels==j]=255
            closing = cv2.morphologyEx(depthx, cv2.MORPH_OPEN, kernel)
            retx,threshx = cv2.threshold(closing,127,255,0)
            drawing = np.zeros((depthx.shape[0], depthx.shape[1], 3), dtype=np.uint8)
            drawing[threshx==255]=255
            drawing = cv2.cvtColor(drawing, cv2.COLOR_BGR2GRAY)
            output2 = cv2.connectedComponentsWithStats(drawing.astype(np.uint8), connectivity, cv2.CV_32S)
            (numLabelsx, labelsx, statsx, centroidsx) = output2
            if numLabelsx>1:
                Amaxx=max(statsx[1:,4])
                for k in range(1,numLabelsx):
                    if statsx[k][4]==Amaxx:
                        drawing = np.zeros((depthx.shape[0], depthx.shape[1], 3), dtype=np.uint8)
                        drawingx= np.zeros((depthx.shape[0], depthx.shape[1], 3), dtype=np.uint8)
                        drawing[closing==255]=255
                        drawingx[closing==255]=255
                        #drawing=closing
                        #drawingx=closing
                        cv2.circle(drawing, (int(centroidsx[k][0]),int(centroidsx[k][1])), 50, (0, 255, 0),1,8,0)
                        p1x=statsx[k][0];p1y=statsx[k][1];
                        p2x=p1x+statsx[k][2];p2y=p1y+statsx[k][3];
                        cv2.rectangle(drawing,(int(p1x),int(p1y)),(int(p2x),int(p2y)),(255,255,0),3)

                        img_gray = cv2.cvtColor(drawingx,cv2.COLOR_BGR2GRAY)
                        ret, thresh = cv2.threshold(img_gray, 127, 255,0)
                        contours, hierarchy = cv2.findContours(thresh,2,1)
                        cnt = contours[0]
                        hull=cv2.convexHull(cnt)
                        per=cv2.arcLength(hull,True)
                        ar=cv2.contourArea(hull,True)
                        cir=(4*np.pi*Amaxx)/(per**2)
                        cvnx=Amaxx/ar
                        wx=" Wbox:"+str("{:.2f}".format(statsx[k][2]))+" "
                        hx=wx+"Hboc:"+str("{:.2f}".format(statsx[k][3]))+" "
                        AR=float(statsx[k][2]) / float(statsx[k][3])
                        R=hx+" R:"+str("{:.2f}".format( float(statsx[k][2]) / float(statsx[k][3]) ))
                        print(per,ar,cir,cvnx)
                        cv2.drawContours(drawing,cnt,-1, (255,0,0),3)
                        cv2.drawContours(drawing,[hull],-1, (255,255,255),2)
                        st='CIR:'+str("{:.2f}".format(cir))+"-"+'CONV:'+str("{:.2f}".format(cvnx))
                        cv2.putText(drawing,str(st)+R,(10,H-50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))
                        print(len(statsx[k]))
                        break
            break
    print('*********************')

    X=(1/(1-2*g1x))*float(centroids[1][0]/W)-(g1x/(1-2*g1x));
    Y=(1/(1-2*g2x))*float(centroids[1][1]/H)-(g2x/(1-2*g2x));
    if X>=1.0:
        X=1.1
    if Y>=1.0:
        Y=1.1
    if X<=0.0:
        X=-0.1
    if Y<=0.0:
        Y=-0.1
    if AR<=1.0:
        ACT=1
        m+=1
    if AR>1.0 and ACT in [1,2]:
        ACT=0
        m=0
    if m>=20 and ACT==1:
        ACT=2
        #m=0
    if cvnx<0.75:
        ACT=3
        m2=0
    if ACT==3:
        if m2>2:
            ACT=0
        else:
            m2+=1


    if X>0 and Y>0 and X<1 and Y<1:
        s=str("{:.2f}".format(X))+" "+str("{:.2f}".format(1-Y))+" "+str(ACT);
    cv2.rectangle(drawing,(int(g1x*W),int(g2x*H)),(int((1-g1x)*W),int((1-g2x)*H)),(0,255,0),3)
    cv2.imshow('Depth', drawing)
    #cv2.imshow('Depth',depth)
    #cv2.imshow('Depth',thresh)
    listener.release(frames)
    print(s)
    return s
############################################################
#print('Press ESC in window to stop')
#w=open(p, 'w')

try:
    from pylibfreenect2 import OpenGLPacketPipeline
    pipeline = OpenGLPacketPipeline()
except:
    try:
        from pylibfreenect2 import OpenCLPacketPipeline
        pipeline = OpenCLPacketPipeline()
    except:
        from pylibfreenect2 import CpuPacketPipeline
        pipeline = CpuPacketPipeline()
#print("Packet pipeline:", type(pipeline).__name__)

# Create and set logger
#logger = createConsoleLogger(LoggerLevel.Debug)
#setGlobalLogger(logger)

fn = Freenect2()
num_devices = fn.enumerateDevices()
if num_devices == 0:
    print("No device connected!")
    sys.exit(1)

serial = fn.getDeviceSerialNumber(0)
device = fn.openDevice(serial, pipeline=pipeline)

listener = SyncMultiFrameListener(
    FrameType.Color | FrameType.Ir | FrameType.Depth)

# Register listeners
device.setColorFrameListener(listener)
device.setIrAndDepthFrameListener(listener)

device.start()

cv2.namedWindow('Depth')
cv2.createTrackbar('threshold', 'Depth', threshold,     100,  change_threshold)
cv2.createTrackbar('g1', 'Depth', g1, 40, change_g1)
cv2.createTrackbar('g2', 'Depth', g2, 40, change_g2)


u=True
FIM=0;
p='fifo'

while u:
        #w=open(p, 'w')
        a=show_depth2()
        if len(a)!=0:
            print(a)
            if j<100:
                w=open(p, 'w')
                w.write(a+'\n')
                w.flush()
                w.close()
            else:
                w=open(p, 'w')
                w.write(a+'\n')
                w.flush()
                w.close()
        j+=1
        print(j)
        if cv2.waitKey(10) == 27:
            #device.stop()
            #device.close()
            #cv2.destroyAllWindows()
            #sys.exit(0)
            break
device.stop()
device.close()
cv2.destroyAllWindows()
sys.exit(0)
