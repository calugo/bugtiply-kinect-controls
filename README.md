# BUGTIPLY KINECT INTERACTIVE PROGRAM

This code is the base for interacting with the evolutionary game **bugtiply** and can be used as a template for further interactive installations.

## Dependencies.

Three scripts are provided one to work with a Xbox  [kinect](https://en.wikipedia.org/wiki/Kinect) 360 sensor, one to operate a Kinect One and to operate a Logitech rumble 2 wireless controller.

The scripts  **btplyx.py** and **bugtiplykin2.py**  are the ones for the Kinect sensore and write to a `fifo` pipe. These require the [freenect](https://github.com/OpenKinect/libfreenect) and [freenect 2](https://github.com/OpenKinect/libfreenect2) drivers respecively and use  [OpenCV](https://opencv.org/) to extract the commands.

The script **joybgtply.py** controls the Rumble pad and requires [pygame](https://www.pygame.org/news).

## Description.

The scripts implement three actions used in the game: select, copy, and kill a bug as well a moving the cursor during the game. The kinect scripts removes the background, isolate the hand connected component and calculates the centroid, area, convex hull and contour to obtain a bounding box, the hand circularity, convexity and box aspect ratio  which can be used to implement a set of commands. The normalised  centroid coordinates and the command are then witten to a `fifo` pipe which is read by the game.

The commands are issued as shown in Fig. 1



<img src="bugtiply.png" alt="bugtiply.png"/>
<figcaption align = "center"><b>Fig.1 Gestures employed to issue commands with the kinect sensors > 1</b></figcaption>


### Background removal.

The script reads frame by frame the depth map from the sensor into a numpy array which is then transform by taking the reciprocal and filtered by a applying a threshold to keep only the values closest to the sensor (Fig. 2)

<img src="bkg.png" alt="BGREMOVAL.png" style="float: center; margin-right: 10px;" />

<figcaption align = "center"><b>Fig.2 - Depth map transformation and backgound  removal thresholding</b></figcaption>

For the 360 device this is done by the snippet:
```
depthx, timestamp = freenect.sync_get_depth()
depthx = np.reciprocal(1.0*depthx)
mx=max(np.ravel(depthx))
depthx=(depthx>(th*mx)).astype(np.uint8)
depthx = np.fliplr(depthx)
depthx = depthx.astype(np.uint8)
```
Which also mirror-reflect the frame. The parameter `th` is adjustable on the fly and is the fraction of the maximum value read by the sensor (the closest object).

For the One device the snippet below achieves a similar result:

```
depth = frames["depth"]
depth=depth.asarray()
mx=max(np.ravel(depth))
depth[depth==0]=mx
depth=np.sqrt(depth)
depth=depth/max(np.ravel(depth))
minx=min(np.ravel(depth))
depth[depth>minx+th]=0.0
depth[depth>0]=1.0
depth = 255*depth.astype(np.uint8)
```

Fig. 3 shows the result of this process, which is similar on both cases.

<img src="bkg2.png" alt="BGREMOVAL2.png"/>

<figcaption align = "center"><b>Fig.4</b></figcaption>

Once this is done. The script searches for the largest connected component and then carries out a closing morphological operation to remove small blobs. Once this is carried out the largest cluster (the hand) is the only present object on screen. Which allow us to compute the area, the bounding box, the centroid and the convex hull.

### Coordinates, Circularity, Convexity and Aspect Ratio.

The coordinates of the centroid $`x_c`$ and $`y_c`$ are scaled as:
```math
X=\frac{x_c}{(1-2g_1)W}-\frac{g_1}{1-2g_1}
```
and:

```math
Y=\frac{y_c}{(1-2g_2)H}-\frac{g_2}{1-2g_2}
```
Where $`g_1`$ and $`g_2`$ are two parameters which determine the fraction of the kinect screen width and height over which the user is able to move the cursor, outside that rectangle these take the value zero or one depending of the location. Once these values are computed the script writes $`X`$ and $`1-Y`$ to the pipe.

The circularity is computed as $`C_R=\frac{4 \pi A_H}{l_H^2}`$ where $`A_H`$ and $`l_H`$ stand for the hand's blob area and perimeter.

The convexity is computed as $`C_{VX}=\frac{A_H}{A_{CH}}`$ where $`A_{CH}`$ is the area of the blob's convex hull.

Another quantity computed is the aspect ratio which is the ratio between the height and width of the blob's bounding box.

Many other descriptors can be computed readily, Figs 4-6 show the case of an open hand with low convexity (0.6) relatively to a closed hand (0.9 and 0.95) whereas the aspect ratio betwen the closed hand are R<1 and R>1 depending of the orientation.

<img src="hand.png" alt="hand.png"/>
<figcaption align = "center"><b>Fig.4 Open hand with circularity and convexity low relatively to the case of a closed hand</b></figcaption>


<img src="fist.png" alt="hand.png"/>
<figcaption align = "center"><b>Fig.5 Closed hand with high circularity and convexity with aspect ratio > 1</b></figcaption>


<img src="fist2.png" alt="hand.png"/>
<figcaption align = "center"><b>Fig.6  Closed hand with high circularity and convexity with aspect ratio < 1</b></figcaption>


### Script in action.

Here's a sample video of the script in action.

![Sample Video](test.mp4)
